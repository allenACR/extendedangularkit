define(['custom'], function(custom) {

	
	var fileName  = 'offcanvas';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $firebase, stBlurredDialog, $timeout, SmoothScroll, $location) {	   
				    


					$scope.offcanvasToggle = function(location){	
					
						   
						   SmoothScroll.$goTo(0).then(function() {
					            	custom.offcanvas('toggle');
					           		stBlurredDialog.close();
					       });
					           	
					       $timeout(function(){					             
					         $location.path("/" + location);  
					       },300);
	
					};

					$scope.close = function(){
					           custom.offcanvas('toggle');
					           stBlurredDialog.close();
					};	

				});				
	    },
	    ///////////////////////////////////////
  };
});
