define("sharedData", ["jquery"], function($) {
  	console.log("Function : sharedData");
	
	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";	
	
	var sharedDataObj = {}; 
        sharedDataObj["logState"] = localStorage.getItem("logState");
        phpJS.getPHPStatus(function(state, data){
        	sharedDataObj["phpStatus"] = JSON.parse(data); 
        });
        
 
		  return {
		  	
		  	///////////////////////////////////////
		  	add:function(key, value) {
                    sharedDataObj[key] = value;
			},
			///////////////////////////////////////	
			
			
		  	///////////////////////////////////////
		  	remove:function(key) {

			},
			///////////////////////////////////////	
			
			
		  	///////////////////////////////////////
		  	getAll:function() {
				return sharedDataObj;
			}
			///////////////////////////////////////							

    		
    
    
  		};
 
});

