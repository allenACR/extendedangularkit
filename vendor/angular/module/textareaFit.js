/**
 * The MIT License
 *
 * Copyright (c) 2014 Nikolas Schmidt-Voigt, http://nikolassv.de
 * @author Nikolas Schmidt-Voigt <nikolas.schmidt-voigt@posteo.de>
 * @module textarea-fit
 */

angular.module("textarea-fit", []).directive("textareaFit", ["$log", function(e) {
    var t = function(e, t) {
        var n = ["width", "font-family", "font-size", "line-height", "min-height", "padding"],
            r = {};
        angular.forEach(n, function(t) {
            r[t] = e.css(t);
        });
        t.css(r)
    };
    return {
        restrict: "A",
        link: function(n, r) {
            if (!angular.isFunction(r.height)) {
                e.error("textareaFit directive only works when jQuery is loaded")
            } else if (!r.is("textarea")) {
                e.info('textareaFit directive only works for elements of type "textarea"')
            } else {
                var i = angular.element("<div>"),
                    s = function() {
                        var e = r.val();
                        if (/\n$/.test(e)) {
                            e += " ";
                        }
                        t(r, i);
                        i.text(e);
                        r.height( (i.height() * .70) + 20 );
                    };
                i.hide().css({
                    "white-space": "pre-wrap",
                    "word-wrap": "break-word"
                });
                r.parent().append(i);
                r.css("overflow", "hidden");
                n.$watch(function() {
                    return r.val()
                }, s);
                n.$watch(function() {
                    return r.width()
                }, s);
                n.$on("destroy", function() {
                    i.remove();
                    i = null
                });
            }
        }
    };
}]);