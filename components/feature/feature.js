define(['custom', 'firebaseSettings', 'papaParse'], function(custom, firebaseSettings) {

	
	var fileName  = 'feature';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $http, $filter, cfpLoadingBar, $firebase, $idle, $q, Linq) {	   
				    
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();	
							
				  			firebaseSettings.getPermissionLevel(function(isUser, details){
				  				$scope.accessLevel = {
				  					isUser: isUser,
				  					permission:  details.type,
				  					value: details.value
				  				};
				  			});		
				  			
				  			// LINQ QUERY
				  			$scope.queryTest();						 
					};	
									
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				    
				    
				    ////////////////////
				    // LINQ SEARCH JSON OBJECT
				    $scope.linqData = [
				    	{"Name":"Allen", 	"Email":"allen@gmail.com", 	"Age": 32 },
				    	{"Name":"Bob", 		"Email":"bob@gmail.com", 	"Age": 20 },
				    	{"Name":"Allen", 	"Email":"allen@gmail.com", 	"Age": 32 },
				    	{"Name":"Bob", 		"Email":"bob@gmail.com", 	"Age": 20 }, 		    	
				    ];
				    
				    $scope.linqData2 = [
				    	{"Name":"Chris", 	"Email":"chris@gmail.com", 	"Age": 16 },
				    	{"Name":"Darel", 	"Email":"darel@gmail.com", 	"Age": 50 },
				    	{"Name":"Egon", 	"Email":"egon@gmail.com", 	"Age": 76 },
				    	{"Name":"Flannery", "Email":"flan@gmail.com", 	"Age": 100 }	
				    ];
				    
				    // BASIC QUERY
	    			$scope.queryTest = function(){
	    			$scope.queryResults = {};
	    			
						    // WHERE (Does not handle numbers well, check for updates)
							$q.all([
									Linq.From($scope.linqData2),
									Linq.Where("(Name != Chris) && (Email != egon@gmail.com) && (Age < 100)")
							]).then(function(results){
									$scope.queryResults.where = results[1];
							});
						    
						    // FIRST
							$q.all([
									Linq.From($scope.linqData),
									Linq.First()
							]).then(function(results){
									$scope.queryResults.first = results[1];
							});
							
						    
						    // PULLS UNIQUE ONES
							$q.all([
									Linq.From($scope.linqData),	
									Linq.Distinct()
							]).then(function(results){
									$scope.queryResults.unique = results[1];	
							});				   
						    
						    // COMBINE - eliminates duplicates
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Union($scope.linqData)
							]).then(function(results){
									$scope.queryResults.combine = results[1];		
							});		
							
						    // ELEMENT AT 
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.ElementAt(3)
							]).then(function(result){
									// console.log( result[1] )	
							});		
							
						    // COUNT
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Count()		 
							]).then(function(result){
									// console.log( result[1] )	
							});							
												
							// SUM
							$q.all([
									Linq.From($scope.linqData),	
									Linq.Sum("Age")	 
							]).then(function(result){
									// console.log( result[1] )	
							});	
		
							// MINIMUM
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Min("Age")
							]).then(function(result){
									// console.log( result[1] )	
							});						
							
							// MAXIMUM
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Max("Age")
							]).then(function(result){
									// console.log( result[1] )	
							});		
							
							// MAXIMUM
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Averange("Age")
							]).then(function(result){
									// console.log( result[1] )	
							});								
								
							// ORDER BY DESCENDING
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.OrderByDesc("Age")
							]).then(function(result){
									// console.log( result[1] )	
							});								
														
							// ORDER BY ASCENDING
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.OrderBy("Age")
							]).then(function(result){
									// console.log( result[1] )	
							});		
							
							// SKIP X NUMBER OF RECORDS
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Skip(2)
							]).then(function(result){
									//console.log( result[1] )	
							});	
		
							// PULL FIRST X NUMBER OF RECORDS
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Take(2)
							]).then(function(result){
									// console.log( result[1] )	
							});
		
							// PULL FIRST X NUMBER OF RECORDS
							$q.all([
									Linq.From($scope.linqData2),	
									Linq.Take(2)
							]).then(function(result){
									// console.log( result[1] )	
							});																			
						    //
				  	};
				    //
				    ////////////////////
				   
				   
				   	////////////////////
					// TEXT EDITOR
				    $scope.myHtml = "";
					$scope.options = {
						placeholder : 'My Placeholder'
					};
			
					$scope.froalaAction = function(action){
						$scope.options.froala(action);
					};
			
					$scope.onPaste = function(e, editor, html){
						return 'Hijacked ' + html;
					}
			
					$scope.onEvent = function(e, editor){
						console.log('onEvent');
					}
					////////////////////
		
		
					////////////////////
					// FORMLY SETUP WITH FIREBASE
						// setup formly entry
						$scope.formFields = [
							{
								type: 'email',
								placeholder: 'placeholder@gmail.com',
								default: 'default@gmail.com',
								key: 'email',
								required: false,
							},
							{
								type: 'text',
								key: 'fName',
								label: 'First Name',
								default: 'defaultFirst',
								placeholder: 'Placeholder first',
								required: false,
							}, 
							{
								type: 'text',
								key: 'lName',
								label: 'Last Name',
								placeholder: 'Placeholder last',
								default: 'defaultLast',
								required: false,
							},
							{
								type: 'textarea',
								key: 'textarea',
								label: 'Textarea',
								placeholder: 'Placeholder textarea!',
								default: 'Default text area',
								lines: 4,
								required: false,
							},					
							{
								type: 'radio',
								key: 'radio',
								label: 'Radio Buttons',
								default: 'valueMaybe',
								required: false,
								options: [
									{
										name: 'YES',
										value: 'valueYes'
									}, {
										name: 'MAYBE',
										value: 'valueMaybe'
									}, {
										name: 'NO',
										value: 'valueNo'
									}
								]
							},
							{
									type: 'text',
									key: 'disabled',
									label: 'Disabled field',
									disabled: true,
									default: 'yes',
									required: false
							}, 
							{
									type: 'number',
									key: 'number',
									label: 'Number 1-10',
									default: 5,
									min: 0,
									max: 10,
									required: false
							},
							{
									type: 'select',
									key: 'select',
									label: 'Select and Options',
									default: 0,
									required: false,
									options: [
										{
											name: 'Car',
											group: 'inefficiently'
										}, {
											name: 'Helicopter',
											group: 'inefficiently'
										}, {
											name: 'Sport Utility Vehicle',
											group: 'inefficiently'
										}, {
											name: 'Bicycle',
											group: 'efficiently'
										}, {
											name: 'Skateboard',
											group: 'efficiently'
										}, {
											name: 'Walk',
											group: 'efficiently'
										}, {
											name: 'Bus',
											group: 'efficiently'
										}, {
											name: 'Scooter',
											group: 'efficiently'
										}, {
											name: 'Train',
											group: 'efficiently'
										}, {
											name: 'Hot Air Baloon',
											group: 'efficiently'
										}
									]
								},
								{
										type: 'password',
										key: 'password',
										label: 'Password',
										required: false,
								}, 
								{
										type: 'checkbox',
										key: 'checkbox',
										label: 'Checkbox',
										default: true,
										required: false,
										
								}																			
																											
						];
						
						
						$scope.makeEditable = false;
						$scope.currentSelection = {};
						$scope.checkTable = function(entry){
							$scope.currentSelection = entry;
						}
						$scope.checkEditable = function(){
							if ($scope.accessLevel.value > 0){
								$scope.makeEditable = !$scope.makeEditable;
							}
							else{
								alert("You must be logged in to edit data.")
							}
						};
						
						
						
						// setup FORMLY in database
						var dbLocation = "formEntries";
						var ref = new Firebase(firebaseSettings.firebaseRoot() + "/" + dbLocation);
		  				var sync = $firebase(ref);
						$scope.allFormEntries = sync.$asArray();
							
			
						// SUBMIT
						$scope.isReady = false; 
						$scope.onSubmit = function onSubmit() {
							
							// check for undefined/nulls/blanks	
							var hasUndefined = _.contains( _.toArray($scope.formData) , undefined)
							var hasNull = _.contains( _.toArray($scope.formData) , null) 
							var hasBlanks = _.contains( _.toArray($scope.formData) , "") 							
							if (hasUndefined || hasNull || hasBlanks){
								alert("All fields must be completed.")
							}
							else{
								// validate logged in and permission level
								if ($scope.accessLevel.value > 0){
									$scope.formData;
									$scope.isReady = true; 
									// add new entry to formly in firebase
									$scope.allFormEntries.$add($scope.formData);
								}		
								else{
									alert("You must be logged in to save information.")
								}
							}
																	
						};		
					//	
					////////////////////

		
				   
				    // SELECT 
				    $scope.singleResultData = [];
					$scope.singleWebBrowsers = [
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Opera...",				maker: "(Opera Software)",		ticked: true	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Internet Explorer...",	maker: "(Microsoft)",			ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Firefox...",				maker: "(Mozilla Foundation)",	ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Safari...",				maker: "(Apple)",				ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Chrome...",				maker: "(Google)",				ticked: false	}
					];
					
				    // MULTI 
				    $scope.multiResultData = [];
					$scope.multiWebBrowsers = [
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Opera...",				maker: "(Opera Software)",		ticked: true	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Internet Explorer...",	maker: "(Microsoft)",			ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Firefox...",				maker: "(Mozilla Foundation)",	ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Safari...",				maker: "(Apple)",				ticked: false	},
					 	{	icon: "<img src='media/select/OmniWeb.png'>",	name: "Chrome...",				maker: "(Google)",				ticked: false	}
					];					


				   	// SELECTION 
					  $scope.selectionStuff = [
					    {selected: false, label: 'Scotchy scotch'},
					    {selected: true, label: 'Monacle'},
					    {selected: true, label: 'Curly mustache'},
					    {selected: false, label: 'Top hat'}
					  ];
				   
				   
				   
					// CHECKLIST 
						$scope.roles = [
						    {id: 1, text: 'guest'},
						    {id: 2, text: 'user'},
						    {id: 3, text: 'customer'},
						    {id: 4, text: 'admin'}
						];
						$scope.user = {
						    roles: [$scope.roles[1]]
						};
						$scope.checkAll = function() {
						    $scope.user.roles = angular.copy($scope.roles);
						};
						$scope.uncheckAll = function() {
						    $scope.user.roles = [];
						};
						$scope.checkFirst = function() {
						    $scope.user.roles.splice(0, $scope.user.roles.length); 
						    $scope.user.roles.push($scope.roles[0]);
						};
					// END CHECKLIST
					
					  	

				
			  		// FORMLY 				  		 
					  	// SETUP
						$scope.formOptions = {
							uniqueFormId: 'simpleform',
							submitCopy: 'Save Info'

						};	
						$scope.formData = {};													  

						// IDLE EVENTS
						$scope.idleEvents = [];
						$scope.idleState = "Active";

			            $scope.$on('$idleStart', function() {
			                // the user appears to have gone idle	
			                $scope.idleEvents.push("Start.");		               
			                $scope.idleEvents.push("Idle: start.");
			            });
			
			            $scope.$on('$idleWarn', function(e, countdown) {
			                // follows after the $idleStart event, but includes a countdown until the user is considered timed out
			                // the countdown arg is the number of seconds remaining until then.
			                // you can change the title or display a warning dialog from here.
			                // you can let them resume their session by calling $idle.watch()
			                $scope.idleEvents.push("Warning. ");
			                $scope.idleState = "Warning";
			            });
			
			            $scope.$on('$idleTimeout', function() {
			                // the user has timed out (meaning idleDuration + warningDuration has passed without any activity)
			                // this is where you'd log them
			                $scope.idleEvents.push("Timeout. ");
			                $scope.idleState = "Timeout";
			            });
			
			            $scope.$on('$idleEnd', function() {
			                // the user has come back from AFK and is doing stuff. if you are warning them, you can use this to hide the dialog
			                $scope.idleEvents.push("End. ");
			                $scope.idleState = "End";
			            });
			
			            $scope.$on('$keepalive', function() {
			                // do something to keep the user's session alive
			                $scope.idleEvents.push("Alive. ");
			                $scope.idleState = "Alive";
			            });					
					
				
	    
				})
				// configure $idle settings
				.config(function($idleProvider, $keepaliveProvider) {
           			$idleProvider.idleDuration(5); // in seconds
          			$idleProvider.warningDuration(5); // in seconds
         		   	$keepaliveProvider.interval(2); // in seconds
		        })
		        // start watching when the app runs. also starts the $keepalive service by default.
		        .run(function($idle){
		          	$idle.watch();
		        });			
	    },
	    ///////////////////////////////////////
  };
});
