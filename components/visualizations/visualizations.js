define(['custom', 'firebaseSettings', 'papaParse'], function(custom, firebaseSettings) {

	
	var fileName  = 'visualizations';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $filter, cfpLoadingBar) {	   
				    
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						custom.transitIn();	
						
						custom.backstretchBG("http://lorempixel.com/abstract/800/640/", 0, 1000);
						custom.backstretchElement( $('#block'), "http://lorempixel.com/city/800/640/", 0, 1000); 
											
						
					
					};	
									
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
						
						
					//  CORNER NAV
				    $scope.positions = ['tl', 'tr', 'br', 'bl'];
				    $scope.effects = [{
				      name: 'Choose an effect here',
				    },{
				      value: 'slidein',
				      name: 'Slide in + fade'
				    },{
				      value: 'zoomin',
				      name: 'Zoom in'
				    },{
				      value: 'fountain',
				      name: 'Fountain'
				    }];
				    $scope.buttons = [{
				      label: 'Home',
				      icon: 'fa fa-home',
				      href: 'home'
				    },{
				      label: 'Page 1',
				      icon: 'fa fa-empire',
				      href: 'page1'
				    },{
				      label: 'Page 2',
				      icon: 'fa fa-rebel',
				      href: 'page2'
				    },{
				      label: 'Page 3',
				      icon: 'fa fa-html5',
				      href: 'page3'
				    }];
				    $scope.chosenEffect = 'zoomin';						
						
					// LINE CHARTS
					$scope.lineData = [
					  {x: 0, value1: 80, value2: 14, value3: 25},
					  {x: 1, value1: 8,  value2: 1 , value3: 50},
					  {x: 2, value1: 55, value2: 11, value3: 75},
					  {x: 3, value1: 16, value2: 147, value3: 100},
					  {x: 4, value1: 90, value2: 87, value3: 125},
					  {x: 5, value1: 42, value2: 45, value3: 150}
					];	
					
					// https://github.com/n3-charts/line-chart
					$scope.lineOptions = {
					  axes: {
					    x: {key: 'x', labelFunction: function(value) {return value;}, type: 'linear', min: 0, max: 5, ticks: 5},
					    y:  {type: 'linear', min: 0, max: 150},
					    y2: {type: 'linear', min: 0, max: 150}
					  },
					  series: [
					    {label: 'Y: Value1', y: 'value1', color: 'steelblue', thickness: '2px', axis: 'y',  type: 'area', striped: true, },
					    {label: 'Y: Value2', y: 'value2', color: 'red', 	  thickness: '2px',	axis: 'y',  type: 'line', visible: true, drawDots: true, dotSize: 5},
					    {label: 'Y: Value3', y: 'value3', color: 'black', 	  					axis: 'y2', type: 'column', visible: true,}
					  ],
					  lineMode: 'linear',
					  tension: 0.7,
					  tooltip: {mode: 'scrubber', formatter: function(x, y, series) {return "x:" + x + " y:" + y;}},
					  drawLegend: true,
					  drawDots: true,
					  columnsHGap: 5
					};					
											
						
					// DONUT 
					$scope.donutData = [
  					  	{ name: 'Adam', age: 29 },
    					{ name: 'Maria', age: 23 },
    					{ name: 'Gabriele', age: 33 },
    					{ name: 'Gabriele', age: 50 },
    					{ name: 'Gabriele', age: 5 }
					];
					
			        $scope.openTooltip = function openTooltip(model) {
			            $scope.selectedModel = model;
			        };
			
			        $scope.closeTooltip = function closeTooltip() {
			            $scope.selectedModel = {};
			        };					
				
			   
					// CHART 
					$scope.chartTypes = [
					     	{name:'pie'},
					        {name:'bar'},
					        {name:'line'},
					        {name:'point'},
 							{name:'area'}
					];
					$scope.chartSelect = $scope.chartTypes[0];
										
					$scope.config = {
					    title: 'Products',
					    tooltips: true,
					    labels: false,
					    mouseover: function() {
					    	//console.log("mouseOver");
					    },
					    mouseout: function() {
					    	//console.log("mouseOut");
					    },
					    click: function() {
					    	//console.log("click");
					    },
					    legend: {
					      display: true,
					      //could be 'left, right'
					      position: 'right'
					    }
					};
					
					$scope.data = {
					    series: ['Sales', 'Income', 'Expense', 'Laptops', 'Keyboards'],
					    data: [{
					      x: "Laptops",
					      y: [100, 500, 0],
					      tooltip: "Custom tooltip."
					    }, {
					      x: "Desktops",
					      y: [300, 100, 100, 600]
					    }, {
					      x: "Mobiles",
					      y: [351]
					    }, {
					      x: "Tablets",
					      y: [54, 0, 200]
					    }]
					};				   
				   
				    
				});
				
	    },
	    ///////////////////////////////////////
  };
});
