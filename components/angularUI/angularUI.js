define(['custom'], function(custom) {

	
	var fileName  = 'angularUI';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, toaster) {	   
				    
				    	$scope.greeting = "AngularUI";

				    	
				    	$scope.contentEdit="<i>interesting</i> stuff";
					
					    $scope.keypressCallback = function($event) {
					        toaster.pop('note', "Keypress", $event.which);					        
					        $event.preventDefault();
					    };
					    
						$scope.items = [
							{ firstName: 'Dean', lastName: 'Sofer',	id: 1, gender: 'Male' },
							{ firstName: 'Dean', lastName: 'Kuntz',	id: 2, gender: 'Male' },
							{ firstName: 'Peter', lastName: 'Piper',id: 3, gender: 'Female' },
							{ firstName: 'Peter', lastName: 'Darwin',id: 4, gender: 'Male' },
							{ firstName: 'Janet', lastName: 'Piper',id: 5, gender: 'Female' },
							{ firstName: 'Dan', lastName: 'Doyon',id: 6, gender: 'Male' },
							{ firstName: 'Andy', lastName: 'Joslin',id: 1, gender: 'Male' },
						];					    
					    
						$scope.inflectorText = 'This is a regular sentence';
						$scope.inflectorType = 'humanize';		    
			   
						$scope.tokenExampleString = "Allen Royston";
						$scope.tokenExample1 = { name:'Bob', subject:'wife' };
				    	$scope.tokenExample2 = ['first','second','third'];
				    	
						$scope.blurCallback = function(evt) {
							toaster.pop('note', "Blur", "");									
						};	
						
						$scope.clickCallback = function(evt){
							toaster.pop('note', "Click", "");	
						};	
						
						$scope.testToaster = function(type){
							
							toaster.pop(type, "Toast successful!", "It works!");
						};
						
	// UI TREE
						$scope.remove = function(scope) {
					      scope.remove();
					    };
					
					    $scope.toggle = function(scope) {
					      scope.toggle();
					    };
					
					    $scope.moveLastToFront = function () {
					      for (i = 1; i <= 2; i++){
					      	var a = $scope['treeData' + i].pop();
					      	$scope['treeData' + i].splice(0,0, a);
					      }
				      
					    };
					
					    $scope.newSubItem = function(scope) {
					      var nodeData = scope.$modelValue;
					      nodeData.nodes.push({
					        id: nodeData.id * 10 + nodeData.nodes.length,
					        title: nodeData.title + '.' + (nodeData.nodes.length + 1),
					        nodes: []
					      });
					    };
					
					    var getRootNodesScope = function() {
					      list = [];
					      $(".tree-root").each(function(){
					      	list.push( $(this) )
					      });
					    	
					      return list;
					    };
					
					    $scope.collapseAll = function() {
					      var list = getRootNodesScope(); 
					      var i = list.length; while(i--){
					      	var scope =   angular.element( getRootNodesScope()[i] ).scope() ;
					      	scope.collapseAll();
					      }
					    };
					
					    $scope.expandAll = function() {
					      var list = getRootNodesScope(); 
					      var i = list.length; while(i--){
					      	var scope =   angular.element( getRootNodesScope()[i] ).scope() ;
					      	scope.expandAll();
					      }
					    };
					
					    $scope.treeData1 = [{
					      "id": 1,
					      "title": "node1",
					      "nodes": [
					        {
					          "id": 11,
					          "title": "node1.1",
					          "nodes": [
					            {
					              "id": 111,
					              "title": "node1.1.1",
					              "nodes": []
					            }
					          ]
					        },
					        {
					          "id": 12,
					          "title": "node1.2",
					          "nodes": []
					        }
					      ],
					    }, {
					      "id": 2,
					      "title": "node2",
					      "nodes": [
					        {
					          "id": 21,
					          "title": "node2.1",
					          "nodes": []
					        },
					        {
					          "id": 22,
					          "title": "node2.2",
					          "nodes": []
					        }
					      ]					    
					    }];  
					    
					    $scope.treeData2 = [{
					      "id": 3,
					      "title": "node3",
					      "nodes": []
					    },
					    {
					      "id": 4,
					      "title": "node4",
					      "nodes": []
					    }];
					  						
						
						// INIT
						$scope.init = function(){
							 cfpLoadingBar.start();
						};
						
						
					    // fake the initial load so first time users can see it right away:
					    $timeout(function() {
					      cfpLoadingBar.complete();
					    }, 750);
				    	
				    	
				});				
	    },
	    ///////////////////////////////////////
  };
});
