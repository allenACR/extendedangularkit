define(['custom', 'firebaseSettings', 'sharedData'], function(custom, firebaseSettings, sharedData) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
			
			
			
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $firebase) {	   
				    
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						console.log(moment().day())	;
						sharedData.add("theWeather", {today: "Sunny"} );
						
						custom.backstretchBG("http://lorempixel.com/city/800/640/", 0, 1000);
						
					};	
					
					
									
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				    
				    

				});
		
	    },
	    ///////////////////////////////////////
  };
});
