

define(['custom', 'firebaseSettings', 'sharedData'], function(custom, firebaseSettings, sharedData) {

	
	var fileName  = 'phpPage';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
			
			
			
				app.controller(fileName + 'Controller', function($scope, $timeout, $http, $filter, cfpLoadingBar, $firebase, $idle, $q, Linq) {	   
				    
				    // GLOBALS
					$scope.inputData = {};
					$scope.inputFinal = {};
					$scope.vEntry = {};
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						$scope.queryString = "SELECT * FROM db";
						$scope.databaseSelect = "codeandl_example";
						$scope.queryResults = {};
						custom.databaseInit(function(status){
							if (status){
								$scope.start();
							}
						});	
					};	
					
					$scope.start = function(){
						$scope.updateField();
					};
					
					$scope.deleteEntry = function(index, queryId){
													
						   	var packet = {
						   		query: "DELETE FROM db WHERE id=" + queryId,
						   		database: $scope.databaseSelect
						   	};
							
			
							phpJS.queryDatabase2(packet, function(state, data){
								if (state){
									$scope.runQuery();
									$scope.$apply();
								}
							});

					};
					
					$scope.viewEntry = function(index){
						$scope.vEntry =  $scope.queryResults[index]; 
					};					
					
					
					$scope.createNewEntry = function(){

						var packet = {
				 			database: $scope.databaseSelect, 
				 			table: "db"
				 		};	
 		
				 		var submitReady = {};
				 		var p = $scope.tableFields;
						
						
						for (var key in p) {
						  if (p.hasOwnProperty(key)) {
						   	if ($scope.inputData[key] != undefined || $scope.inputData[key] != null){							   		
						   		
						   		var type = p[key].Type;
						   		var field = $scope.inputData[key].replace(/"/g, "&quot;").replace(/'/g, "&lsquo;");
						   			
						   		// wrap any of these text fields 
						   		if (type.indexOf("char") > -1 || 
						   		    type.indexOf("text") > -1 ||
						   		    type.indexOf("binary") > -1 ||
						   		    type.indexOf("blob") > -1 ||
						   		    type.indexOf("enum") > -1 ||
						   		    type.indexOf("set") > -1 
						   		){
						   			console.log('WRAP');
						   			field = "\"" + field + "\"";
						   		}
						   		else{
						   			field = parseInt(field);	
						   		}
						   		
						   		submitReady[p[key].Field] = field;
		
						   	}
						  }
						}	

				 		var valuePacket = JSON.stringify(submitReady);

				 		console.log(valuePacket);
						phpJS.createNewEntry(packet, valuePacket, function(state, data){	
 							if (state){
								$scope.newlyCreated = data; 
								$scope.$apply();
 							}
 							else{
 								$scope.tableFields = null;
 							}							

						});

						
					};
					
					$scope.updateField = function(){						
						
						
						var packet = {
				 			database: $scope.databaseSelect, 
				 			table: "db"
				 		};						
						phpJS.getTableFields(packet, function(state, data){
							
							if (state){
								$scope.tableFields = data;
								$scope.$apply();
							}
							else{
								$scope.tableFields = null;
							}
						});	
					};
					
					
					$scope.fileUpload = function(){	
						var file = $('#uploadSource')[0].files[0];	
							phpJS.basicUpload(file, "fileName", "images/allen", function(state, data){
									console.log(state, data);
							});	

					};
					
					$scope.sendEmail = function(){
							var emailData = {
								sendTo: "allen.acr@gmail.com",
								subject: "My Subject JSON",
								content: "<h3>Hello there!</h3><br><p>I am the content!</p>",
								replyTo: "no-replay@codeandlogic.com"	
							};
						
							phpJS.sendEmail(emailData, function(state, data){
									console.log(state, data);
							});	
					};
					
					$scope.runQuery = function(){
						   	var packet = {
						   		query: $scope.queryString,
						   		database: $scope.databaseSelect
						   	};

							phpJS.queryDatabase(packet, function(state, data){
								if (state){
									$scope.queryResults = data;
									$scope.$apply();
								}
							});							
					};
					
									
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				    
				    

				});
		
	    },
	    ///////////////////////////////////////
  };
});
