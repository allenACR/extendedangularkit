<div class="" ng-init="init()">

	<!-- PAGE NAME -->
	<div class="row">
		<div class="small-12 columns">
				<h1>PHP Stuff</h1>				
				<br>
		</div>		
		<hr>
	</div>
	
	<div class="row">
		<div class="small-12 columns">
				<h3>PHP Upload Image to Server</h3>				
				<br>	
				<form id='uploadForm' enctype="multipart/form-data">
				    Select image to upload:
				    <input id='uploadSource' type="file">
				</form>	
				<button ng-click='fileUpload()'>File Upload Start</button>			
				
		</div>	
		<hr>	
	</div>


	<div class="row" ng-if="phpStatus.status == 'server'">
		<div class="small-12 columns">
				<h3>PHP Send Email</h3>				
				<br>
			
				<button ng-click='sendEmail()'>Send Test Email</button>			
				
		</div>	
		<hr>	
	</div>
	
	<div class="row">
		<div class="small-12 columns">
			<div class='small-6 pull-left'>
				<h3>PHP Run Query</h3>				
				<br>
				<label>Query String</label>
				<input class='small-6' ng-model='queryString' placeholder='QUERY string'/><br>
				<select class='small-6' ng-model='databaseSelect' placeholder='Select a Database' ng-change='updateField()'>
					<option value='codeandl_example'>Example</option>
					<option value='codeandl_religion'>Religion</option>
				</select> <br>
				<button ng-click='runQuery()' >Run Query</button>	
			</div>

			
			<div class='small-6 slate-gradient pull-right' >
				<json-explorer json-data="{{vEntry}}"></json-explorer>
			</div>
			

			<div class='small-12 pull-left'>
				<div ng-repeat="query in queryResults">
					{{$index}}: 
					<button class='btn tiny' ng-click='viewEntry($index, query.id)'><i class="fa fa-newspaper-o"></i></button> 
					<button class='btn alert tiny' ng-click='deleteEntry($index, query.id)'><i class="fa fa-times-circle"></i></button>
				</div>
				<br><br>
			</div>
			

			
		</div>	
		<hr>	
	</div>	
	
	
	<div class="row" ng-if='tableFields != null'>
		<div class="small-12 columns">
			<div class='small-6 pull-left'>
				<h3>PHP Create New Record</h3>				
				<br>
				<div ng-repeat="field in tableFields" ng-if='field.Key != "PRI"'>

					<label><strong>{{field.Field}}</strong> [{{field.Type}}]: </label><input ng-model='inputData[$index]' >

				
				</div>
				<button ng-click='createNewEntry()'>Run Query</button>	
			</div>
			

			<div class='small-6 slate-gradient pull-right' >
				<json-explorer json-data="{{inputData}}"></json-explorer>
				<br>
				<div ng-if='newlyCreated.status == "good"'>
					<hr>
					<h3>New ID: {{newlyCreated.createdId}}</h3>
				</div>
				
			</div>		
		
		<hr>
		</div>	
	</div>		

			
</div>	

